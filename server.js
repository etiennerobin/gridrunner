var express= require('express'); 
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var clients = [];
var nbParty = 0;
var waitingForPlayers = [];
var parties = [];
var nbPlayer = 0;

process.env.PWD = process.cwd();
app.use(express.static(process.env.PWD + '/'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + 'index.html');
});

io.on('connection', function (socket) {
    console.log('user joined ');
    clients[socket.id] = socket;
    nbPlayer++;
    socket.partyNumber = null;
    socket.bLeader = false;

    socket.on('message', function (message) {
        console.log(message);
    });

    socket.on('multiplayer', function (){
        if(waitingForPlayers.length === 0){
            socket.partyNumber = nbParty;
            nbParty++;
            socket.bLeader = true;
            parties[socket.partyNumber] = {player1: socket.id, player2: null, started: false};
            waitingForPlayers.unshift(socket.partyNumber);
            console.log("New Party : " + socket.partyNumber);
            socket.emit('isLeader');
        }
        else {
            socket.partyNumber = waitingForPlayers.pop();
            parties[socket.partyNumber].player2 = socket.id;
            clients[parties[socket.partyNumber].player1].emit('playerJoin');
            socket.emit('isFollower');
        }
    });

    socket.on('updatePosition', function(data){
        if(parties[socket.partyNumber] === undefined) return;
        var teammate = socket.bLeader ? parties[socket.partyNumber].player2 : parties[socket.partyNumber].player1;
        if(teammate !== null) {
            clients[teammate].emit('updatePosition', data);
        }
    });

    socket.on('singleplayer', function(){
        if(socket.partyNumber !== null){
            if(!socket.bLeader){
                clients[parties[socket.partyNumber].player1].emit('playerLeave');
                parties[socket.partyNumber].player2 = null;
                if(!parties[socket.partyNumber].started) waitingForPlayers.unshift(socket.partyNumber);
            }
            else {
                if(parties[socket.partyNumber].player2 !== null){
                    clients[parties[socket.partyNumber].player2].emit('cancel');
                }
                if(waitingForPlayers.indexOf(socket.partyNumber) !== -1){
                    waitingForPlayers.splice(waitingForPlayers.indexOf(socket.partyNumber), 1);
                }
            }
        }
    });

    socket.on('start', function(){
        if(parties[socket.partyNumber] === undefined) return;
        parties[socket.partyNumber].started = true;
        if(parties[socket.partyNumber].player1 !== null) clients[parties[socket.partyNumber].player1].emit('start');
        if(parties[socket.partyNumber].player2 !== null) clients[parties[socket.partyNumber].player2].emit('start');
    });

    socket.on('disconnect', function () {
        nbPlayer--;
        if(socket.partyNumber !== null){
            if(!socket.bLeader){
                clients[parties[socket.partyNumber].player1].emit('playerLeave');
                parties[socket.partyNumber].player2 = null;
                if(!parties[socket.partyNumber].started) waitingForPlayers.unshift(socket.partyNumber);
            }
            else {
                if(parties[socket.partyNumber].player2 !== null){
                    clients[parties[socket.partyNumber].player2].emit('cancel');
                }
                if(waitingForPlayers.indexOf(socket.partyNumber) !== -1){
                    waitingForPlayers.splice(waitingForPlayers.indexOf(socket.partyNumber), 1);
                }
            }
        }
    });

});

var port = 8080;

http.listen(port, function () {
    console.log('listening on *:' + port);
});