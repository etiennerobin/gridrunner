# Gridrunner #

Ceci est un projet javascript, fait pour le cours de Web Science donné par Michel Buffa à l'université nice sophia antipolis.

### Sujet ###

Refaire le jeu [Gridrunner](http://minotaurproject.co.uk/Minotaur/gridrunner.php), multi-joueurs, multi-rooms et tournant sur navigateur.

### Installation ###

* Cloner le projet
* [Installer NodeJS](https://nodejs.org/download/)
* Lancer *node server.js* dans une console
* [Jouer!](http://localhost:8080/#/)

### TODO ###

* Problèmes de synchronisation entre clients
* Rajouter des niveaux
* Régler la difficulté du jeu
* Effectuer les déplacement en fonction des ticks
* Le rendre responsive

### Contributions ###

* Sons et images de Jeff Minter
* Librairie de son [Howler](https://github.com/goldfire/howler.js/)

### Remerciements ###

* [Jeff Minter](http://minotaurproject.co.uk/about.php)
* [Michel Buffa](http://users.polytech.unice.fr/~buffa/)